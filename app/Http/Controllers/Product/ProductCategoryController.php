<?php

namespace App\Http\Controllers\Product;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCategoryController extends ApiController
{


    public function __construct()
    {
        $this->middleware('client.credentials')->only(['index']);

    }

    // Obtengo cateogrías de un producto

    public function index(Product $product)
    {
        $categories = $product->categories;

        return $this->showAll($categories);
    }


    // Actualiza categorpia de producto

    public function update(Request $request, Product $product, Category $category)
    {
       $product->categories()->syncWithoutDetaching([$category->id]);

       return $this->showAll($product->categories);
        
    }

    public function destroy(Product $product, Category $category)
    {
        if (!$product->categories()->find($category->id)) {
            return $this->errorResponse('La categoría especificada 
            no es una categoría de este producto', 404);
        }

        $product->categories()->detach([$category->id]);

        return $this->showAll($product->categories);
    }
}
