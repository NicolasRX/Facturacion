<?php

namespace App;

use App\Transformers\TransactionTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Buyer;
use App\Product;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'quantity',
        'buyer_id',
        'product_id',
    ];

    public $transaction = TransactionTransformer::class;

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
        //Una transaction pertenece a un comprador
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
        //y pertenece a un producto
    }

}
